//
//  GTLTasksTasks+Additions.m
//  tasks
//
//  Created by Elad Lebovitch on 6/26/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "GTLTasksTasks+Additions.h"

@implementation GTLTasksTasks (Additions)

- (GTLTasksTask*) parentTaskForTask:(GTLTasksTask*)task
{
    return [self.items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"identifier LIKE %@", task.parent]].lastObject;
}

@end
