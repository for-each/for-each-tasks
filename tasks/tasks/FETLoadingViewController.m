//
//  FETLoadingViewController.m
//  tasks
//
//  Created by Elad Lebovitch on 6/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETLoadingViewController.h"
#import "FETAppDelegate.h"
#import "FETSideMenuViewController.h"

@interface FETLoadingViewController ()

@end

@implementation FETLoadingViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserverForName:NOTIF_TASK_LIST_UPDATED object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note)
     {
         FETAppDelegate *delegate = (FETAppDelegate *)[[UIApplication sharedApplication] delegate];
         FETSideMenuViewController* sideMenu = [self.storyboard instantiateViewControllerWithIdentifier:@"FETRootViewController"];
         [delegate.window addSubview:sideMenu.view];
         [self.view removeFromSuperview];
     }];
    
    [appData loadInitialData];
}

@end
