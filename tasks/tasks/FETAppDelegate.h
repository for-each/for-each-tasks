//
//  FETAppDelegate.h
//  tasks
//
//  Created by Elad Lebovitch on 6/16/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FETAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
