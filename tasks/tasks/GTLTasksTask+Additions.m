//
//  GTLTasksTask+Additions.m
//  tasks
//
//  Created by Elad Lebovitch on 6/25/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "GTLTasksTask+Additions.h"
#import "GTLTasksTasks+Additions.h"

@implementation GTLTasksTask (Additions)
@dynamic isTaskComplete;

- (NSInteger) indentionLevelInTasksList:(GTLTasksTasks*)tasks
{
    NSInteger currLevel = 0;
    
    GTLTasksTask* currTask = self;

    // Find it's level
    while (currTask.parent != nil)
    {
        ++currLevel;
        currTask = [tasks parentTaskForTask:currTask];
    }
    
    return currLevel;
}

- (NSInteger) maxIndetionLevelInTaskList:(GTLTasksTasks*)tasks
{
    // Get the current index
    NSInteger index = [tasks.items indexOfObject:self] - 1;
    NSInteger maxIndentionLevel = [self indentionLevelInTasksList:tasks];
    
    // Make sure this isn't the first item on the list
    if (index > 0)
    {
        GTLTasksTask* task  = tasks.items[index];
        
        // Check the current task
        while (index > 0 && task.parent != nil)
        {
            NSInteger tempIndentionLevel = [task indentionLevelInTasksList:tasks];
            
            // Update the min max
            if  (tempIndentionLevel > maxIndentionLevel)
            {
                maxIndentionLevel = tempIndentionLevel;
            }
            
            // Next task please
            --index;
            task = tasks.items[index];
        }
    }
    
    // This task can be one more
    return maxIndentionLevel;
}

- (GTLTasksTask*)parentTaskForIndentionLevel:(NSInteger)level inList:(GTLTasksTasks*)tasks
{
    NSInteger index = [tasks.items indexOfObject:self] - 1;
    GTLTasksTask* parentTask = nil;
    
    
    // Check the current task
    while (index >= 0 && parentTask == nil && level > 0)
    {
        GTLTasksTask* task = tasks.items[index];

        // If this is the first task that fits our level requirement
        if ([task indentionLevelInTasksList:tasks] == level - 1)
        {
            // Save it
            parentTask = task;
        }
        
        // Next task please
        --index;
    }
    
    // This task can be one more
    return parentTask;
}

+ (GTLTasksTask*) patchObjectWithTask:(GTLTasksTask*)task
{
    GTLTasksTask* patchObject = [GTLTasksTask object];
    patchObject.identifier = task.identifier;
    
    return patchObject;
}

- (BOOL)isIsTaskComplete
{
    static NSString* const completeStatus = @"complete";
    return [self.status isEqualToString:completeStatus];
}

@end
