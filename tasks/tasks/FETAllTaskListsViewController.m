//
//  FETAllTaskListsViewController.m
//  tasks
//
//  Created by Elad Lebovitch on 6/21/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETAllTaskListsViewController.h"
#import "FETTaskListCell.h"

@interface FETAllTaskListsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *taskListTable;

@end

@implementation FETAllTaskListsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Register for task list updates
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataPresentedOnScreen) name:NOTIF_TASK_LIST_UPDATED object:nil];
}

- (void)refreshDataPresentedOnScreen
{
    [self.taskListTable reloadData];
}

#pragma mark UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appData.userTasksList.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* const cellIdentifier = @"FETTaskListCell";
    FETTaskListCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.taskList = appData.userTasksList.items[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    appData.currentTaskList = appData.userTasksList[indexPath.row];
}

@end