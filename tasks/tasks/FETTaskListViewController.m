//
//  FETAllTasksViewController.m
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETTaskListViewController.h"
#import "FETTaskListCell.h"
#import "FETSingleTaskCell.h"
#import "FETSingleTaskViewController.h"


@interface FETTaskListViewController () <UITableViewDataSource, UITableViewDelegate, FETSingleTaskCellDelegate>

// Outlets
@property (weak, nonatomic) IBOutlet UITableView *tasksTable;
@property (weak, nonatomic) IBOutlet UILabel *taskListTitle;
@property (nonatomic, strong) GTLTasksTasks     *currentTaskListTasks;
@end

@implementation FETTaskListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Register for task list updates
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retrieveDataFromServer) name:NOTIF_CURRENT_TASK_LIST_CHANGED object:nil];
}

- (void)retrieveDataFromServer
{
    // Reload data
    [tasksManager getTasksForList:appData.currentTaskList completion:^(id googleAnswer)
    {
        self.currentTaskListTasks = googleAnswer;
        [self refreshDataPresentedOnScreen];
    }];
}

- (void)refreshDataPresentedOnScreen
{
    [super refreshDataPresentedOnScreen];
    self.taskListTitle.text = appData.currentTaskList.title;
    [self.tasksTable reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // If we are navigating to a single task
    if ([segue.identifier isEqualToString:@"FETSingleTaskSegue"])
    {
        FETSingleTaskViewController* taskView = segue.destinationViewController;
        taskView.task = sender;
    }
}

#pragma mark TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.currentTaskListTasks.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* const cellIdentifier = @"FETSingleTaskCell";
    
    FETSingleTaskCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    // Create a new task
    if (cell == nil)
    {
        cell = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil][0];
        cell.delegate = self;
    }
    
    // Set the task and indention level:
    cell.task = self.currentTaskListTasks.items[indexPath.row];
    cell.indentationLevel = [cell.task indentionLevelInTasksList:self.currentTaskListTasks];    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"FETSingleTaskSegue" sender:self.currentTaskListTasks.items[indexPath.row]];
}

#pragma mark FETSingleTaskCellDelegate

- (void) taskCellDidSwipeRight:(FETSingleTaskCell*)taskCell
{
    // Get the data
    NSIndexPath* path = [self.tasksTable indexPathForCell:taskCell];
    
    // Indent task:
    NSInteger currIndetionLevel = [taskCell.task indentionLevelInTasksList:self.currentTaskListTasks];
    
    
    
    // Only if we can find a suitable father
    if (currIndetionLevel + 1 <= [taskCell.task maxIndetionLevelInTaskList:self.currentTaskListTasks] + 1 &&
        path.row > 0)
    {
        // Getting the parent
        GTLTasksTask* parent = [taskCell.task parentTaskForIndentionLevel:currIndetionLevel + 1
                                                                   inList:self.currentTaskListTasks];
        
        if  (parent != nil)
        {
            [tasksManager moveTask:taskCell.task toTask:parent];
            [self.tasksTable reloadData];
        }
    }
    else
    {
        NSLog(@"Could not move task %@ with indention %d and max %d", taskCell.task.title, currIndetionLevel, [taskCell.task maxIndetionLevelInTaskList:self.currentTaskListTasks]);
    }
    
    // Reload table
    [self.tasksTable reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
}

- (void) taskCellDidSwipeLeft:(FETSingleTaskCell*)taskCell
{
    
}

- (void) taskCellDidTapComplete:(FETSingleTaskCell*)taskCell
{
    // Get the data
    NSIndexPath* path = [self.tasksTable indexPathForCell:taskCell];
    
    NSMutableArray* newArray = [NSMutableArray arrayWithArray:self.currentTaskListTasks.items] ;
    [newArray removeObject:taskCell.task];
    
    self.currentTaskListTasks.items = newArray;
    
    // Perform delete
    [self.tasksTable deleteRowsAtIndexPaths:@[path]
                           withRowAnimation:UITableViewRowAnimationMiddle];
    [tasksManager completeTask:taskCell.task];
}



@end
