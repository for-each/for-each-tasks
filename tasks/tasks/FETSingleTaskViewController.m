//
//  FETSingleTaskViewController.m
//  tasks
//
//  Created by Elad Lebovitch on 6/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETSingleTaskViewController.h"
#import <FEHelpers/FECategories.h>

@interface FETSingleTaskViewController ()
@property (weak, nonatomic) IBOutlet UITextField *taskTitle;
@property (weak, nonatomic) IBOutlet UITextView *taskDescription;
@property (weak, nonatomic) IBOutlet UITextField *taskDueDate;

@end

@implementation FETSingleTaskViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)refreshDataPresentedOnScreen
{
    self.taskTitle.text = self.task.title;
    self.taskDescription.text = self.task.notes;
    self.taskDueDate.text = [self.task.due.date prettyDate];
}

- (IBAction)completeTaskPressed:(id)sender
{
    [tasksManager completeTask:self.task];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
