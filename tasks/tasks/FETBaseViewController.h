//
//  FETBaseViewController.h
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FETBaseViewController : UIViewController

@property (nonatomic) BOOL needsRetrieveDataFromServer;

- (void) retrieveDataFromServer;
- (void) refreshDataPresentedOnScreen;

@end
