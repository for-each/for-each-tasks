//
//  FETTaskTableViewCell.h
//  tasks
//
//  Created by Elad Lebovitch on 6/20/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FETSingleTaskCell;

@protocol FETSingleTaskCellDelegate <NSObject>

- (void) taskCellDidSwipeRight:(FETSingleTaskCell*)taskCell;
- (void) taskCellDidSwipeLeft:(FETSingleTaskCell*)taskCell;
- (void) taskCellDidTapComplete:(FETSingleTaskCell*)taskCell;

@end

@interface FETSingleTaskCell : UITableViewCell

@property (nonatomic, weak) id<FETSingleTaskCellDelegate> delegate;
@property (nonatomic, strong) GTLTasksTask* task;

@end
