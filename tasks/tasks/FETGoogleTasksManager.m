//
//  FETGoogleTasksManager.m
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETGoogleTasksManager.h"
#import <Google-API-Client/GTLPlus.h>

@interface FETGoogleTasksManager ()

@property (nonatomic, strong) GTMOAuth2Authentication *auth;
@property (nonatomic, strong) GTLServiceTasks *tasksService;

@end

@implementation FETGoogleTasksManager

static NSString* const kGoogleTasksClientID = @"353060691997.apps.googleusercontent.com";
static NSString* const kGoogleTasksKeyChaingItemName = @"FETKeychainItemName";
static NSString* const kGoogleTasksSecret =  @"jzJ0NNuc8Kxx0pABnN7lfxRw";

// Consts by Google
NSString *const kTaskStatusCompleted = @"completed";
NSString *const kTaskStatusNeedsAction = @"needsAction";

- (id)init
{
    self = [super init];
    if (self)
    {
        // Setup a task service
        self.tasksService = [GTLServiceTasks new];
        
        // Get the current auth item (if any)
        self.auth = [GTMOAuth2ViewControllerTouch
                     authForGoogleFromKeychainForName:kGoogleTasksKeyChaingItemName
                     clientID:kGoogleTasksClientID
                     clientSecret:kGoogleTasksSecret];

    }
    return self;
}

#pragma mark Authenticaion
- (void)authenticateIfNeeded:(FETBaseCompletionBlock)completion
{
    // Check if we need to auth
    if (self.auth.canAuthorize)
    {
        self.tasksService.authorizer = self.auth;
        
        // Getting user details
        [self retrieveUserDetails];
        
        // Finished
        if  (completion)
        {
            completion(nil);
        }
    }
    // We have to present a login
    else
    {
        // Setup a completion handler for google's oauth
        GTMOAuth2ViewControllerCompletionHandler completeHandler = ^(GTMOAuth2ViewControllerTouch *viewController,
                                                                     GTMOAuth2Authentication *auth,
                                                                     NSError *error)
        {
            // Make sure there were no errors
            if (error == nil)
            {
                self.auth = auth;
                self.tasksService.authorizer = self.auth;
                [flowManager.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                
                // Getting user details
                [self retrieveUserDetails];
                
                completion(nil);
            }
        };
        
        // Create and show the OAuth 2 sign-in controller
        GTMOAuth2ViewControllerTouch *authController =
        [GTMOAuth2ViewControllerTouch controllerWithScope: [GTMOAuth2Authentication scopeWithStrings:kGTLAuthScopePlusMe, kGTLAuthScopeTasks, nil]
                                                 clientID:kGoogleTasksClientID
                                             clientSecret:kGoogleTasksSecret
                                         keychainItemName:kGoogleTasksKeyChaingItemName
                                        completionHandler:completeHandler];
     
        // Present inside a nabigation controller
        UINavigationController* controller = [[UINavigationController alloc] initWithRootViewController:authController];
        authController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissLoginView)];
        
        // Present it
        [flowManager.presentedViewController presentViewController:controller
                                                          animated:YES
                                                        completion:nil];
    }
}

- (void) retrieveUserDetails
{
    // Get a plusService
    GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
    plusService.authorizer = self.auth;
    
    // Get the user's details as well
    GTLQueryPlus *profileQuery = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:profileQuery completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error)
    {
        if (error == nil)
        {
            appData.userProfile = object;
        }
    }];
    
    
}

- (void) dismissLoginView
{
    [flowManager.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Data Methods

- (void)getAllTaskLists:(FETBaseCompletionBlock)completion
{
    GTLQueryTasks *query = [GTLQueryTasks queryForTasklistsList];
    
    [self.tasksService executeQuery:query
                  completionHandler:^(GTLServiceTicket *ticket,
                                      id taskLists, NSError *error)
     {
         // No errors
         if  (error == nil)
         {
             completion(taskLists);
         }
     }];
}

#pragma mark Tasks methods

- (void) getTasksForList:(GTLTasksTaskList*)list completion:(FETBaseCompletionBlock)completion
{
    [self getTasksForList:list completion:completion showCompleted:NO];
}

- (void) updateTask:(GTLTasksTask*)patchObject
         completion:(void (^)(GTLServiceTicket *ticket, id object, NSError *error))completion
{
    // Make the query
    GTLQueryTasks* query = [GTLQueryTasks queryForTasksPatchWithObject:patchObject
                                                              tasklist:appData.currentTaskList.identifier
                                                                  task:patchObject.identifier];
    [self.tasksService executeQuery:query
                  completionHandler:completion];
}

- (void) completeTask:(GTLTasksTask*)task
{
    // Create a patch object
    GTLTasksTask *patchObject = [GTLTasksTask patchObjectWithTask:task];
    patchObject.status = kTaskStatusCompleted;
    
    NSString* currentTaskListIdentifier = appData.currentTaskList.identifier;
    
    // Make the query
    [self updateTask:patchObject completion:^(GTLServiceTicket *ticket, id object, NSError *error)
    {
        // Now clear the task lists
        [self.tasksService executeQuery:[GTLQueryTasks queryForTasksClearWithTasklist:currentTaskListIdentifier]
                      completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_CURRENT_TASK_LIST_CHANGED object:nil];
        }];
    }];
}

- (void) getTasksForList:(GTLTasksTaskList*)list completion:(FETBaseCompletionBlock)completion showCompleted:(BOOL)showCompleted
{
    // Check if we got a list
    if (list)
    {
        // Create a querty
        GTLQueryTasks *query = nil;
        
        // Show completed tasks?
        query = [GTLQueryTasks queryForTasksListWithTasklist:list.identifier];
        
        // Return the data
        [self.tasksService executeQuery:query
                      completionHandler:^(GTLServiceTicket *ticket,
                                          id tasks, NSError *error)
         {
             // Make sure we didn't get an error
             if (error == nil)
             {
                 completion(tasks);
             }
         }];
    }
}

- (void) moveTask:(GTLTasksTask*)task toTask:(GTLTasksTask*)otherTask
{
    // Move the task to another parent (or nil - to top)
    task.parent = otherTask.identifier;
    
    NSLog(@"Moving task %@ under task %@", task.title, otherTask.title);
    
    GTLQueryTasks* query = [GTLQueryTasks queryForTasksMoveWithTasklist:appData.currentTaskList.identifier
                                                                   task:task.identifier];
    query.parent = otherTask.identifier;
    [self.tasksService executeQuery:query completionHandler:nil];
}

#pragma mark Properties

- (NSString *)firstName
{
    return self.auth.userEmail;
}

@end
