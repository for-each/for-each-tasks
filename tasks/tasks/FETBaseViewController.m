//
//  FETBaseViewController.m
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETBaseViewController.h"

@interface FETBaseViewController ()

@end

@implementation FETBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Auto retrieve
    self.needsRetrieveDataFromServer = YES;

    if (flowManager.presentedViewController == nil)
    {
        flowManager.presentedViewController = self;
    }    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    flowManager.presentedViewController = self;
    
    [self refreshDataPresentedOnScreen];
    
    if  (self.needsRetrieveDataFromServer)
    {
        [self retrieveDataFromServer];
    }
}

- (void) retrieveDataFromServer
{
    self.needsRetrieveDataFromServer = NO;
}

- (void) refreshDataPresentedOnScreen
{
    
}


@end
