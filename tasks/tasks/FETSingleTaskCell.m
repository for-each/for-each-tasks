//
//  FETTaskTableViewCell.m
//  tasks
//
//  Created by Elad Lebovitch on 6/20/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETSingleTaskCell.h"

@interface FETSingleTaskCell ()
@property (weak, nonatomic) IBOutlet UITextField *taskTitle;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;

@end

@implementation FETSingleTaskCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)setTask:(GTLTasksTask *)task
{
    _task = task;
    [self refreshData];
}

- (void) refreshData
{
    self.taskTitle.text = self.task.title;
    if ([self.task.status isEqualToString:@"completed"])
    {
        self.taskTitle.textColor = [UIColor greenColor];
    }
    
    self.completeButton.selected = self.task.isTaskComplete;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    float indentPoints = self.indentationLevel * self.indentationWidth;
    
    self.contentView.frame = CGRectMake(
                                        indentPoints,
                                        self.contentView.frame.origin.y,
                                        self.contentView.frame.size.width - indentPoints,
                                        self.contentView.frame.size.height
                                        );
}

#pragma mark User Events

- (IBAction)completeButtonTapped:(id)sender
{
    [self.delegate taskCellDidTapComplete:self];
}


- (IBAction)userSwipeRight:(id)sender
{
    [self.delegate taskCellDidSwipeRight:self];
}

- (IBAction)userSwipeLeft:(id)sender
{
    [self.delegate taskCellDidSwipeLeft:self];
}


@end
