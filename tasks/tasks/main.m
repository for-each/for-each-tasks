//
//  main.m
//  tasks
//
//  Created by Elad Lebovitch on 6/16/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FETAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FETAppDelegate class]));
    }
}
