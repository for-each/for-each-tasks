//
//  FETSideMenuViewController.m
//  tasks
//
//  Created by Elad Lebovitch on 6/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETSideMenuViewController.h"
#import "FETTaskListCell.h"

@interface FETSideMenuViewController () <SASlideMenuDataSource, SASlideMenuDelegate>
@property (weak, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

@end

@implementation FETSideMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Register for task list updates
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataPresentedOnScreen) name:NOTIF_TASK_LIST_UPDATED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataPresentedOnScreen) name:NOTIF_USER_DETAILS_UPDATED object:nil];
    
    self.slideMenuDataSource = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshDataPresentedOnScreen];
}

- (void)refreshDataPresentedOnScreen
{
    self.firstNameLabel.text = appData.userProfile.displayName;
    self.emailLabel.text = appData.signedInEmail;
    self.userImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:appData.userProfile.image.url]]];
    
    [self.tableView reloadData];
}

#pragma mark SASideMenu subclasses

-(NSIndexPath*) selectedIndexPath
{
    return [NSIndexPath indexPathForRow:0 inSection:0];
}

- (NSString *)segueIdForIndexPath:(NSIndexPath *)indexPath
{
    // This it the tasks section
    if (indexPath.section == 0)
    {
        return @"FETTaskListSegue";
    }
    else
    {
        return nil;
    }
}

-(void) configureMenuButton:(UIButton *)menuButton
{
    menuButton.frame = CGRectMake(0, 0, 60, 29);
    [menuButton setTitle:@"< Lists" forState:UIControlStateNormal];
    [menuButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
}

- (Boolean)disablePanGestureForIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appData.userTasksList.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* const cellIdentifier = @"FETTaskListCell";
    FETTaskListCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.taskList = appData.userTasksList.items[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    appData.currentTaskList = appData.userTasksList[indexPath.row];
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}


@end
