//
//  GTLTasksTasks+Additions.h
//  tasks
//
//  Created by Elad Lebovitch on 6/26/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "GTLTasksTasks.h"

@interface GTLTasksTasks (Additions)

- (GTLTasksTask*) parentTaskForTask:(GTLTasksTask*)task;

@end
