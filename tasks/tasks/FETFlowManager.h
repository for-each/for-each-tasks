//
//  FETFlowManager.h
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FEFlowManager.h"

#define flowManager [FETFlowManager sharedInstance]

@interface FETFlowManager : FEFlowManager

@end
