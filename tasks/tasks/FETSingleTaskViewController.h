//
//  FETSingleTaskViewController.h
//  tasks
//
//  Created by Elad Lebovitch on 6/28/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETBaseViewController.h"
#import <GTLTasksTask.h>

@interface FETSingleTaskViewController : FETBaseViewController

@property (nonatomic, strong) GTLTasksTask* task;

@end
