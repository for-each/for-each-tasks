//
//  FETGoogleTasksManager.h
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Google-API-Client/GTLTasks.h>
#import <Google-API-Client/GTLUtilities.h>
#import <GTMHTTPFetcher/GTMHTTPFetcherLogging.h>
#import <gtm-oauth2/GTMOAuth2ViewControllerTouch.h>
#import "GTLTasksTask+Additions.h"

typedef void (^FETBaseCompletionBlock)(id googleAnswer);

@interface FETGoogleTasksManager : NSObject

- (void) authenticateIfNeeded:(FETBaseCompletionBlock)completion;
- (void) getAllTaskLists:(FETBaseCompletionBlock)completion;
- (void) getTasksForList:(GTLTasksTaskList*)list completion:(FETBaseCompletionBlock)completion;

// Data Modifications
- (void) completeTask:(GTLTasksTask*)task;
- (void) moveTask:(GTLTasksTask*)task toTask:(GTLTasksTask*)otherTask;

// Data properties
@property (nonatomic, weak,readonly) NSString* firstName;
@property (nonatomic, weak,readonly) NSString* userEmail;

@end
