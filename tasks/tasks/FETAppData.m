//
//  FETAppData.m
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETAppData.h"

@implementation FETAppData


NSString* const NOTIF_USER_DETAILS_UPDATED = @"NOTIF_USER_DETAILS_UPDATED";
NSString* const NOTIF_TASK_LIST_UPDATED = @"NOTIF_TASK_LIST_UPDATED";
NSString* const NOTIF_CURRENT_TASK_LIST_CHANGED = @"NOTIF_CURRENT_TASK_LIST_CHANGED";

static FETAppData* sharedAppData;

+ (FETAppData*) sharedInstance
{
    if (sharedAppData == nil)
    {
        sharedAppData = [FETAppData new];
    }
    
    return sharedAppData;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.sharedTasksManager = [FETGoogleTasksManager new];
    }
    return self;
}

- (void) loadInitialData
{
    // Authenticate on this view load
    [self.sharedTasksManager authenticateIfNeeded:^(id googleAnswer)
     {
         // Now get the user data
         [self.sharedTasksManager getAllTaskLists:^(id googleAnswer)
          {
              // Save the google task list
              self.userTasksList = googleAnswer;
              
              if (self.userTasksList.items.count > 0)
              {
                  // Select the first item
                  self.currentTaskList = self.userTasksList.items[0];
              }
          }];
     }];
}

- (void)setUserTasksList:(GTLTasksTaskLists *)userTasksList
{
    _userTasksList = userTasksList;
    
    // Post a notification with the new task list
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_TASK_LIST_UPDATED object:nil userInfo:@{@"list": userTasksList}];
}

- (void)setCurrentTaskList:(GTLTasksTaskList *)currentTaskList
{
    _currentTaskList = currentTaskList;
    
    // Post a notification with the new task list
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_CURRENT_TASK_LIST_CHANGED object:nil userInfo:@{@"list": currentTaskList}];
}

- (void)setUserProfile:(GTLPlusPerson *)userProfile
{
    _userProfile = userProfile;
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_USER_DETAILS_UPDATED object:nil];
}

- (NSString*) signedInEmail
{
    return self.sharedTasksManager.userEmail;
}

@end
