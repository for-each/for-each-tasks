//
//  FETAppData.h
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Google-API-Client/GTLPlus.h>
#import <GTMHTTPFetcherLogging.h>
#import <GTLPlusPerson.h>
#import "FETGoogleTasksManager.h"

#define appData [FETAppData sharedInstance]
#define tasksManager appData.sharedTasksManager

extern NSString* const NOTIF_TASK_LIST_UPDATED;
extern NSString* const NOTIF_CURRENT_TASK_LIST_CHANGED;
extern NSString* const NOTIF_USER_DETAILS_UPDATED;

@interface FETAppData : NSObject

+ (FETAppData*) sharedInstance;

// Loading
- (void) loadInitialData;

// User Info
@property (nonatomic, strong) GTLPlusPerson* userProfile;
@property (nonatomic, strong) NSString* signedInEmail;

// Tasks
@property (nonatomic, strong) GTLTasksTaskLists *userTasksList;
@property (nonatomic, strong) GTLTasksTaskList  *currentTaskList;
@property (nonatomic, strong) FETGoogleTasksManager* sharedTasksManager;

@end
