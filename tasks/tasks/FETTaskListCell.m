//
//  FETTaskListsItem.m
//  tasks
//
//  Created by Elad Lebovitch on 6/19/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "FETTaskListCell.h"

@interface FETTaskListCell ()
@property (weak, nonatomic) IBOutlet UILabel *listName;


@end

@implementation FETTaskListCell

- (void)setTaskList:(GTLTasksTaskList *)taskList
{
    _taskList = taskList;
    [self refreshData];
}

- (void) refreshData
{
    self.listName.text = self.taskList.title;
}

@end
