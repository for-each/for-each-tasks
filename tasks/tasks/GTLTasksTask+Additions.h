//
//  GTLTasksTask+Additions.h
//  tasks
//
//  Created by Elad Lebovitch on 6/25/13.
//  Copyright (c) 2013 for-each. All rights reserved.
//

#import "GTLTasksTask.h"
#import <GTLTasksTasks.h>

@interface GTLTasksTask (Additions)

+ (GTLTasksTask*) patchObjectWithTask:(GTLTasksTask*)task;

- (NSInteger) indentionLevelInTasksList:(GTLTasksTasks*)tasks;
- (NSInteger) maxIndetionLevelInTaskList:(GTLTasksTasks*)tasks;
- (GTLTasksTask*)parentTaskForIndentionLevel:(NSInteger)level inList:(GTLTasksTasks*)tasks;

@property (nonatomic, readonly) BOOL isTaskComplete;
@end
